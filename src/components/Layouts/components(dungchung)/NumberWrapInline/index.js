import "./style.css"
const NumberWrapInline = ({number}) => {
    return (
        <div className="number">
            <span>{number}</span>
        </div>
    )
}
export default NumberWrapInline;