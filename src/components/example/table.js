<table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
<thead className="text-xs text-white uppercase bg-red-700">
    <tr>
        <th scope="col" className="px-6 py-3">
            Product name
        </th>
        <th scope="col" className="px-6 py-3">
            Color
        </th>
        <th scope="col" className="px-6 py-3">
            Category
        </th>
        <th scope="col" className="px-6 py-3">
            Price
        </th>
        <th scope="col" className="px-6 py-3">
            <span className="sr-only">Edit</span>
        </th>
    </tr>
</thead>
<tbody>
    <tr className="bg-white border-b border-gray-700 text-gray-700 hover:text-red-800">
        <td className="px-6 py-4">Apple MacBook Pro 17"</td>
        <td className="px-6 py-4">Silver</td>
        <td className="px-6 py-4">Laptop</td>
        <td className="px-6 py-4">$2999</td>
        <td className="px-6 py-4 text-right">
            <a
                href="#"
                className="font-medium text-blue-600 dark:text-blue-500 hover:underline"
            >
                Edit
            </a>
        </td>
    </tr>
</tbody>
</table>