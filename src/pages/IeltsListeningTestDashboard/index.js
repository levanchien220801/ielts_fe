import { useState } from "react";
import AddIeltsListeningTest from "./IeltsListeningTestDetail"
import IeltsListeningTestList from "./IeltsListeningTestList"

export const IeltsListeningTestDashboard = () => {
    return (
        <div>
            <IeltsListeningTestList></IeltsListeningTestList>
        </div>
    )
}

export default IeltsListeningTestDashboard