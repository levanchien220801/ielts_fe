export const HomeIeltsTips = () => {
    return (
        <div
            className="d-flex bg-gray-200 align-items-center flex-column p-4"
            style={{
                height: "660px",
                boxShadow: "5px 5px 10px rgba(0, 0, 0, 0.3);",
            }}
        >
            <div className="" style={{ width: "100%" }}>
                <span className="fs-1 fw-bold text-red-700 mt-4 ml-16">
                    IELTS Tips
                </span>
            </div>

            <div
                className="bg-white p-4 mt-8 d-flex justify-content-around align-items-center"
                style={{
                    height: "85%",
                    width: "94%",
                    border: "5px black solid",
                    borderRadius: "50px",
                    flexWrap: "wrap",
                }}
            >
                <div
                    className="d-flex align-items-center p-4 bg-gray-100"
                    style={{
                        width: "48%",
                        height: "48%",
                        border: "3px black solid",
                        borderRadius: "10px",
                    }}
                >
                    <div
                        className="border-red-700 border-2 p-1"
                        style={{
                            borderRadius: "10px",
                            boxSizing: "border-box",
                        }}
                    >
                        <img
                            src="https://ielts.s3.ap-southeast-2.amazonaws.com/asdasdasd.png"
                            style={{
                                display: "block",
                                height: "auto",
                                width: "180px",
                            }}
                        ></img>
                    </div>

                    <div
                        className="ml-6 d-flex flex-column"
                        style={{ flex: "1 0 100px" }}
                    >
                        <div className="">
                            <span className="fs-5 fw-bold">
                                Do's and Don'ts in IELTS Writing Task 2
                            </span>
                        </div>
                        <div>
                            <span>
                                IELTS Writing Task 2 is the second task of your
                                IELTS Writing test. Here, you will be presented
                                with an essay topic and you will be ....
                            </span>
                        </div>
                    </div>
                </div>
                <div
                    className="d-flex align-items-center p-4 bg-gray-100"
                    style={{
                        width: "48%",
                        height: "48%",
                        border: "3px black solid",
                        borderRadius: "10px",
                    }}
                >
                    <div
                        className="border-red-700 border-2 p-1"
                        style={{
                            borderRadius: "10px",
                            boxSizing: "border-box",
                        }}
                    >
                        <img
                            src="https://ielts.s3.ap-southeast-2.amazonaws.com/asdasd.png"
                            style={{
                                display: "block",
                                height: "auto",
                                width: "180px",
                            }}
                        ></img>
                    </div>

                    <div
                        className="ml-6 d-flex flex-column"
                        style={{ flex: "1 0 100px" }}
                    >
                        <div className="">
                            <span className="fs-5 fw-bold">
                                Collocations about business reports
                            </span>
                        </div>
                        <div>
                            <span>
                                Here, you will be presented with an essay topic
                                and you will be ....IELTS Writing Task 2 is the
                                second task of your IELTS Writing test.
                            </span>
                        </div>
                    </div>
                </div>
                <div
                    className="d-flex align-items-center p-4 bg-gray-100"
                    style={{
                        width: "48%",
                        height: "48%",
                        border: "3px black solid",
                        borderRadius: "10px",
                    }}
                >
                    <div
                        className="border-red-700 border-2 p-1"
                        style={{
                            borderRadius: "10px",
                            boxSizing: "border-box",
                        }}
                    >
                        <img
                            src="https://ielts.s3.ap-southeast-2.amazonaws.com/asdasdasadsadsads.png"
                            style={{
                                display: "block",
                                height: "auto",
                                width: "180px",
                            }}
                        ></img>
                    </div>

                    <div
                        className="ml-6 d-flex flex-column"
                        style={{ flex: "1 0 100px" }}
                    >
                        <div className="">
                            <span className="fs-5 fw-bold">
                                10 techniques for IELTS Listening
                            </span>
                        </div>
                        <div>
                            <span>
                                These 10 IELTS Listening Tips provide you with
                                essential strategies to help you get the score
                                you need in the exam and show you how to improve
                                IELTS listening.
                            </span>
                        </div>
                    </div>
                </div>
                <div
                    className="d-flex align-items-center p-4 bg-gray-100"
                    style={{
                        width: "48%",
                        height: "48%",
                        border: "3px black solid",
                        borderRadius: "10px",
                    }}
                >
                    <div
                        className="border-red-700 border-2 p-1"
                        style={{
                            borderRadius: "10px",
                            boxSizing: "border-box",
                        }}
                    >
                        <img
                            src="https://ielts.s3.ap-southeast-2.amazonaws.com/matching.png"
                            style={{
                                display: "block",
                                height: "auto",
                                width: "180px",
                            }}
                        ></img>
                    </div>

                    <div
                        className="ml-6 d-flex flex-column"
                        style={{ flex: "1 0 100px" }}
                    >
                        <div className="">
                            <span className="fs-5 fw-bold">
                                Matching Heading Type of Questions
                            </span>
                        </div>
                        <div>
                            <span>
                                One of the tricky questions apart from multiple
                                choice questions in IELTS listening are the
                                matching heading type of questions. As it can be
                                determined by the term itself, ...
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
